<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function submitForm(Request $request){
        $senderEmail = "info@rapidmux.com";
        $senderName  ='Rapid Mux';
        $UserEmail = $request->email;
        ContactUs::createData($request->all());
        Mail::send('mails.contact_us', array('data' => $request->all()), function ($message) use ($senderEmail, $senderName , $UserEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($UserEmail)
                ->subject('Rapid Mux');
        });
        return collect([
            'status' => true,
            'message' => 'Thanks For Contacting US'
        ]);
    }
}
